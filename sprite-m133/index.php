<?php require_once 'manage-sprites.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Halloween</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <form action="#" method="post">
        <main>
            <h1>Gestion de sprites en session</h1>
            <div id="playground">
                <?php echo showSpriteImage($_SESSION['sprites']) ?>
            </div>
            <div id="panel">
               <fieldset>
                    <legend>Déplacer le sprite sélectionné</legend>
                    <?php echo showSpriteList($_SESSION['sprites'], $id); ?>
                    <button type="submit" name="action" class="moveButton" value="left">  ⇦ </button>
                    <button type="submit" name="action" class="moveButton" value="up">    ⇧ </button>
                    <button type="submit" name="action" class="moveButton" value="down">  ⇩ </button>
                    <button type="submit" name="action" class="moveButton" value="right"> ⇨ </button>
                </fieldset>

                <fieldset>
                    <legend>Ajouter un sprite à la session</legend>
                    <p><label>Nom  :</label><input type="text" name="name" value="<?= $name ?>"></p>
                    <p><label>posX :</label><input type="text" name="posX" value="<?= $posX ?>"></p>
                    <p><label>posY :</label><input type="text" name="posY" value="<?= $posY ?>"></p>
                    <button type="submit" name="action" class="addButton" value="addSprite"> Ajouter </button>
                </fieldset>
            </div>
        </main>
    </form>
</body>
</html>
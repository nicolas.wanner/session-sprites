<?php
/**
 * Gestion de la session
 */
session_start();

// Mettre $initSession à TRUE pour réinitialiser la session
$initSession = FALSE;

// Initialiser la session lors de la première ouverture
if ($initSession || !isset($_SESSION['sprites'])) {
    $_SESSION = [];
    $_SESSION['sprites'] = [];

    // Ajouter les sprites par défaut
    addSprite(newSprite('Gérard',    10,  50));
    addSprite(newSprite('Liliane',  100, 250));
    addSprite(newSprite('Isabelle', 200, 150));
}

/**
 * Vérifier si l'ordre de déplacement est valide
 * 
 * @param  String $move, le déplacement à valider
 * @return boolean TRUE si valide autrement FALSE
 */
function isMoveAction($move) {
    return ($move == 'up') || ($move == 'down') || ($move == 'left') || ($move == 'right');
}

/**
 * Vérifier si le numéro de sprite correspond à un sprite existant
 * 
 * @param integer $id = Identifiant du sprite
 * @return boolean TRUE si valide, autrement FALSE
 */
function isIdValid($id) {
    return isset($_SESSION['sprites'][$id]);
}

/**
 * Indique si un valeur filtrée existe et est valide
 *
 * @param mixed $value = La valeur à vérifier
 * @return boolean TRUE si valide autrement FALSE
 */
function isValid($value) {
    return ($value !== NULL) && ($value !== FALSE);
}

/**
 * Créer et initialiser la structure de donnée pour un sprite
 * 
 * @param  String  $name  Nom du sprite
 * @param  Integer $left  Position X du sprite
 * @param  Integer $top   Position Y du sprite
 * @return Le tableau associatif pour le sprite
 */
function newSprite($name, $left, $top) {
    return [
        "id"   => -1,
        "name" => $name,
        "posX" => $left,
        "posY" => $top,
    ];
}

/**
 * Ajouter un sprite à la session.
 * 
 * @param Array $sprite = La structure du sprite à ajouter
 */
function addSprite($sprite) {
    if (isSpriteValid($sprite)) {
        $id = array_key_last($_SESSION['sprites']);
        $id = ($id === null) ? 0 : $id + 1;
        $sprite['id'] = $id;
        $_SESSION['sprites'][] = $sprite;
        return TRUE;
    }
    return FALSE;
}

/**
 * Générer le composant pour sélectionner un sprite dans la liste
 *
 * @param Array   $sprites = Tableau des sprites
 * @param Integer $current = Id du sprite sélectionné
 * @return String Le HTML pour afficher le <select><option>
 */
function showSpriteList($sprites, $current) {
    $result = "<select name=\"spriteId\">\n";
    foreach ($sprites as $index => $sprite) {
        $selected = ($index == $current) ? 'selected' : '';
        $result  .= "<option value=\"$index\" $selected>${sprite['name']}</option>\n";
    }
    $result .= "</select>\n";
    return $result;
}

/**
 * Générer l'affichage des sprites sous forme "graphique"
 *
 * @param Array $sprites = Liste des sprites à afficher
 * @return void
 */
function showSpriteImage($sprites) {
    $result = "\n";
    foreach ($sprites as $index => $sprite) {
        $name = $sprite['name'];
        $pos  = "left:${sprite['posX']}px; top:${sprite['posY']}px;";
        $result .= "<div class=\"sprite\" style=\"$pos\"><span class=\"label\">$name</span></div>\n";
    }
    return $result;
}

/**
 * Filtrer le nom du sprite
 * 
 * @param  String  $value = Le nom du sprite
 * @return mixed  Retourne le nom s'il est valide, autrement NULL
 */
function filterName($value) {
    if (!isValid($value) || ($value == '')) return NULL;
    // TODO: check for existing name
    return $value;
}

/**
 * Filter un valeur de position (X ou Y)
 * @param  Integer $value = La coordonée à vérifier
 * @return mixed  Retourne la coordonée si elle est valide, autrement NULL
 */
function filterPosition($value) {
    if (!isValid($value) || ($value < 0) && ($value > 626)) return NULL;
    return $value;
}

/**
 * Vérifier si un sprite est valide
 * 
 * @param  Array  $sprite La structure du sprite
 * @return boolean TRUE si valide, autrement FALSE
 */
function isSpriteValid(&$sprite) {
    return (filterName($sprite['name'])     !== NULL) &&
           (filterPosition($sprite['posX']) !== NULL) &&
           (filterPosition($sprite['posY']) !== NULL);
}

/**
 * Déplacer le sprite d'un incrément dans la direction demandée
 * 
 * @param  Array  &$sprite   Le sprite à déplacer
 * @param  String $direction La direction du déplacement
 */
function moveSprite(&$sprite, $direction) {
    if ($direction == 'left') {
        $sprite['posX'] = max($sprite['posX'] - 5, 0);
    }
    else if ($direction == 'right') {
        $sprite['posX'] = min($sprite['posX'] + 5, 626);
    }
    else if ($direction == 'up') {
        $sprite['posY'] = max($sprite['posY'] - 5, 0);
    }
    else if ($direction == 'down') {
        $sprite['posY'] = min($sprite['posY'] + 5, 626);
    }    
}

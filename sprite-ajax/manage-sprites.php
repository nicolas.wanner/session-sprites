<?php
// Ce script envoie tous les sprites sous la forme d'une liste JSON
require_once 'php/application.php';

// Lire le formulaire
$action = filter_input(INPUT_POST, 'action');
$id     = filter_input(INPUT_POST, 'spriteId', FILTER_VALIDATE_INT);
$name   = filter_input(INPUT_POST, 'name');
$posX   = filter_input(INPUT_POST, 'posX', FILTER_VALIDATE_INT);
$posY   = filter_input(INPUT_POST, 'posY', FILTER_VALIDATE_INT);

// Traiter le formulaire
if (isMoveAction($action) && isIdValid($id)) {
    // Déplacer le sprite indiqué, dans la direction indiquée
    moveSprite($_SESSION['sprites'][$id], $action);
}
else if ($action == 'addSprite') {
    // Ajouter un sprite dans la liste s'il est valide
    if (addSprite(newSprite($name, $posX, $posY))) {
        // Effacer le formulaire si le sprite est ajouté
        $name = $posX = $posY = NULL;
    }
}

// Si le script est appellé par la page d'index, on retourne rien
// sinon on retourne un joli fichier JSON contenant les Sprites
if (basename($_SERVER['PHP_SELF']) != 'index.php') {
    header('Content-Type: application/json');
    echo json_encode($_SESSION['sprites'], JSON_PRETTY_PRINT);
}

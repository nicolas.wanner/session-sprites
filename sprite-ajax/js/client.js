/**
 * Gestion des sprites côté client
 */
"use strict";

// Variables globales ---------------------------------------------------------
let submitButton = null;        // Dernier submit button pressé
let spriteList = [];            // Mes sprites
let currentSprite = 0;          // Le sprite actif

/**
 * Début du programme
 */
window.addEventListener('load', function(event) {
    // Détourner les événement "onsubmit" des forms pour les faire passer en arrière plan
    let forms = document.querySelectorAll('form');
    forms.forEach( form => {
        form.addEventListener('submit', event => {
            ajaxForm(event);
        });
    });

    // Détecter les clicks sur les boutons submit pour imiter le comportement
    // des formulaires pour le PHP
    let buttons = document.querySelectorAll('button[type="submit"]');
    buttons.forEach(btn => {
        btn.addEventListener('click', event => {
            submitButton = event.target;
        })
    });

    // Mettre à jour le sprite actif quand la valeur choisie dans la liste change
    document.querySelector('select[name="spriteId"]').addEventListener('change', event => {
        currentSprite = parseInt(event.target.value);
        updateScreen();
    });

    // Force le sprite 0 a être sélectionné (appel bidon pour éviter unused function dans jshint)
    selectSprite(0);

    // Forcer le chargement de sprites puis les afficher
    displaySprites();
})

/**
 * Transforme un objet sprite en HTML pour l'affichage graphique
 */
function spriteToImage(sprite, active) {
    let selected     = active ? ' selected' : '';
    let spriteClass  = `class="sprite${selected}"`;
    let spriteStyle  = `style="left:${sprite.posX}px; top:${sprite.posY}px"`;
    let spriteEvents = `onmousedown="startDrag(event);" `+
                       `onmouseup="stopDrag(event);"` + 
                       `onclick="selectSprite(${sprite.id})"`;

    return `<div data-id="${sprite.id}" ${spriteClass} ${spriteEvents} ${spriteStyle}>` + 
            `<span>${sprite.name}</span>` + 
            `</div>\n`;
}

/**
 * Transforme un objet sprite en HTML pour l'affichage <option>
 */
function spriteToOption(sprite, active) {
    let selected = active ? 'selected' : '';
    return `<option value="${sprite.id}" ${selected}>` + 
           sprite.name + 
           `</option>\n`;
}

/**
 * Transforme la liste de sprite en HTML selon le callback donné
 */
function spritesToHTML(callback) {
    let htmlString   = '';
    spriteList.forEach(sprite => {
        htmlString += callback(sprite, (currentSprite == sprite.id));
    });
    return htmlString;
}

/**
 * Mettre à jour l'affichage des sprites
 */
function updateScreen() {
    document.getElementById('playground').innerHTML     = spritesToHTML(spriteToImage);
    document.getElementsByName('spriteId')[0].innerHTML = spritesToHTML(spriteToOption);
}

/**
 * Récupérer la liste des citrouilles du serveur et afficher
 */
async function displaySprites() {
    let response = await fetch('manage-sprites.php');
    spriteList = await response.json();
    updateScreen();
}

/**
 * Envoyer le formulaire en arrière plan au serveur
 */
async function ajaxForm(event) {
    // Ne pas laisser le navigateur envoyer le formulaire
    event.preventDefault();

    // Les données de la forme
    let formData = new FormData(event.target);

    // Ajouter le bouton submit qui a été pressé
    formData.append(submitButton.name, submitButton.value);

    // Mettre à jour le sprite actuel
    currentSprite = parseInt(event.target.elements['spriteId'].value);

    // Communiquer avec le serveur
    let response = await fetch(`manage-sprites.php`, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        body: new URLSearchParams(formData)
    });

    // Traduire la réponse en objet
    spriteList = await response.json();

    // Mettre à jour l'affichage
    updateScreen();
}

/**
 * Callback pour sélectionner une citrouille à l'aide d'un click sur l'image
 */
function selectSprite(id) {
    currentSprite = id;
    updateScreen();
}


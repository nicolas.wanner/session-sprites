<!DOCTYPE html>
<html>
<head>
    <title>Sprite PHP / Ajax</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/client.js"></script>
</head>
<body>
    <form action="#" method="post" ONsubmit="ajaxForm(event)">
        <main>
            <h1>Gestion de sprites en session</h1>
            <div id="playground">
            </div>
            <div id="panel">
               <fieldset>
                    <legend>Déplacer le sprite sélectionné</legend>
                    <select name="spriteId"></select>
                    <button type="submit" name="action" class="moveButton" value="left">  ⇦ </button>
                    <button type="submit" name="action" class="moveButton" value="up">    ⇧ </button>
                    <button type="submit" name="action" class="moveButton" value="down">  ⇩ </button>
                    <button type="submit" name="action" class="moveButton" value="right"> ⇨ </button>
                </fieldset>

                <fieldset>
                    <legend>Ajouter un sprite à la session</legend>
                    <p><label>Nom  :</label><input type="text" name="name"></p>
                    <p><label>posX :</label><input type="text" name="posX"></p>
                    <p><label>posY :</label><input type="text" name="posY"></p>
                    <button type="submit" name="action" class="addButton" value="addSprite"> Ajouter </button>
                </fieldset>
            </div>
        </main>
    </form>
</body>
</html>
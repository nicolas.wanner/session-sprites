// Gestion des routes relative aux sprites
'use strict';

// Pour la gestion du site et des routes
const express = require('express');
const router  = express.Router();

// Les constantes pour mon application

const MIN_X = 0;
const MAX_X = 500;

const MIN_Y = 0;
const MAX_Y = 500;

const STEP = 10;

// Initialiser la session si ce n'est pas déjà fait
function getSpriteFromSession(req) {

    if (false || !req.session.spriteList) {
      req.session.spriteList = [
        {id:0, name: "Gérard",    posX:  10,  posY:  10},
        {id:1, name: "Liliane",   posX: 100,  posY: 200},
        {id:2, name: "Géraldine", posX: 200,  posY: 100},
      ];
    }
  
    return req.session.spriteList;
}

// Borner la valeur entre un minimum et un maximum
function limitValue(value, minimum, maximum) {
    return (value < minimum) ? minimum : ((value >maximum) ? maximum : value);
}

// Récupérer la liste des citrouilles
router.get('/', function(req,res){
    let sprites = getSpriteFromSession(req);
    res.send(JSON.stringify(sprites));
});

// Déplacer un sprite puis retourner la liste des sprites
// Passage des paramètres par un formulaire de type POST
router.post('/move', function(req,res) {
    // Données de la session
    let sprites = getSpriteFromSession(req);

    // Lire paramètre depuis formulaire POST
    let id  = parseInt(req.body.spriteId);
    let dir = req.body.action;

    // Valider les paramètres, dir n'est pas testé car la méthode move le fait
    if (!isNaN(id) && sprites[id] !== undefined) {
        switch(dir) {
        case 'up' :
            sprites[id].posY = limitValue(sprites[id].posY - STEP, MIN_Y, MAX_Y);
            break;
        case 'down'  :
            sprites[id].posY = limitValue(sprites[id].posY + STEP, MIN_Y, MAX_Y);
            break;
        case 'left'  :
            sprites[id].posX = limitValue(sprites[id].posX - STEP, MIN_X, MAX_X);
            break;
        case 'right' :
            sprites[id].posX = limitValue(sprites[id].posX + STEP, MIN_X, MAX_X);
            break;
        }
    }

    // Retourner la liste des citrouilles dans la session
    res.send(JSON.stringify(sprites));
});
  
// Ajouter une citrouille à la session
// Passage des paramètres par un formulaire de type POST
router.post('/add', function(req,res) {
    let sprites = getSpriteFromSession(req);

    // Lire paramètre depuis URL
    let name = req.body.name;
    let posX = parseInt(req.body.posX);
    let posY = parseInt(req.body.posY);
    let id   = sprites.length;

    // Valider les paramètres
    if (!isNaN(posX) && !isNaN(posY) && name !== '') {
        let duplicate = false;

        for( let sprite of sprites ){
            if (sprite.name == name) {
                duplicate = true;
                break;
            }
        }

        if (!duplicate) {
            sprites[id] = {
                id:   id,
                name: name,
                posX: limitValue(posX, MIN_X, MAX_X),
                posY: limitValue(posY, MIN_Y, MAX_Y),
            };    
        }
    }

    // Retourner la liste des citrouilles dans la session
    res.send(JSON.stringify(sprites));
});

// Retourner les routes configurées au programme principal
module.exports = router;

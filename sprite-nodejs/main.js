/**
 * Programme principal pour une application web avec session
 */
"use strict";

// Importer le serveur web et ses modules complémentaires
const express = require('express');
const cookie  = require('cookie-parser');
const session = require('express-session');
const sqlite  = require('connect-sqlite3')(session);

// Créer une instance du serveur web
const app = express();

// Assurer que le serveur retourne les ressources statiques
// tels que page web, feuilles de styles, javascripts, images, ...
app.use(express.static('public'));

// Faire en sorte que le serveur utilise les extensions importées
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookie());

// Configurer le fonctionnement de la session
app.use(session({
  store: new sqlite(),
  secret: 'your secret',        // A remplacer par un truc au hasard
  cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 } // 1 week
}));

// Route spéciale pour effacer la session
// c'est pas obligatoire mais ça peu bien servir
app.use('/reset', function(req, res) {
  // Détruire la session
  req.session.destroy();
  // Afficher la page d'accueil
  res.redirect('/index.html');
});



// ----------------------------------------------------------------------------
// >>> Début de la partie spécifique à l'application

// Importer les routes relatives au sprites
const sprites = require('./sprites.js');
app.use('/sprite', sprites);

// <<< Fin de la partie spécifique à l'application
// ----------------------------------------------------------------------------



// Démarrer le serveur sur le port indiqué
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});
